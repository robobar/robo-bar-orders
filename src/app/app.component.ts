import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  orders$: Observable<any>;

  constructor(private api: ApiService, public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.getOrders();
    setInterval(() => {
      this.getOrders();
    }, 10000)
  }

  getOrders() {
    this.orders$ = this.api.getOrders();
  }

  deleteOrder(orderId) {
    this.api.deleteOrder(orderId).subscribe(res => {
      this.getOrders();
    });
  }
}
