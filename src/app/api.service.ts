import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  endPoint = 'https://i2e9ymi5v4.execute-api.us-east-2.amazonaws.com/dev';

  constructor(private http: HttpClient) { }

  getOrders() {
    return this.http.get(`${this.endPoint}/orders`)
      .pipe(map((res: any) => res.orders));
  }

  deleteOrder(orderId) {
    return this.http.delete(`${this.endPoint}/orders/${orderId}`);
  }
}